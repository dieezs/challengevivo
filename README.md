## Vivo Challenge - Diego Magalhaes de Souza

### Technologies used

- NodeJS
- Typescript
- Express: webserver
- Celebrate: middleware validation
- Esbuild: build bundler
- Esbuild-runner: to speed up development environment
- Nodemon: watch changes in dev environment
- Pino logger: for pretty logs
- Database: Postgres
- ORM: Prisma
- Tests: Jest for unit tests
- Docker and Docker Compose

#### Others

- Eslint
- Prettier

# Run the application

You must install Docker in your computer to run the application. We're going to create one docker container to run the database.

If you don't have already installed Docker in your machine <a href="https://docs.docker.com/engine/install/">click here</a> and follow the instructions to install it.

## Run the postgres container

` docker run --name postgres -e POSTGRES_PASSWORD=admin -e POSTGRES_USER=admin -e POSTGRES_DB=vivo-challenge -p 5432:5432 -d postgres`

Check out if the container is running running the command below...

` docker ps`

## Run the backend application

Be sure that you are at ./ level of the code repository.

`cd backend`

`yarn install`

`yarn setup-prisma`

`yarn dev`

### Running tests

Some tests might fail due to direct connect to the database. If eventually fails because of the `beforeEach`, run again. It's a tech debt to mock the tests with the database.

`yarn test:unit`

### Running eslint

`yarn eslint`

### REST Endpoints

In this repository, contains the postman collection to the project. You could use it.

`Vivo Challenge.postman_collection`

### Generate UUID to use REST Endpoints

In this challenge, I use the <a href="https://www.uuidgenerator.net/">uuid generator</a> to generate some uuid to use in `conversationId` and `from` JSON properties.

## Desirable

- Build a frontend application to connect with the project. I'd do with React and Tailwindcss.
- To improve real time "conversation" with the Bot, would be nice to implement websocket.
- Implement CI/CD, using Jenkins or GithubActions.
- After implementation of Docker compose, I'd use ECS at the cloud to manage the containers needed(backend, database and maybe the frontend). I don't have professional experience with ECS but I'd give it a try.
- Implement AI to the "Bot". The bot should be able to answer a question from the user.
- Maybe machine learning to get and analyze the most frequently questions and answer with the most accuracy possible.
- To fault tolerance, first of all I'd deploy the backend application and the database to a region closest to our target user and the most important in multi AZ deployment, at least 2 az.
  To do that, we could use a load balancer to do the job of which AZ the request of our user should access.
- To improve high availability, I'd use auto scaling group for EC2 to scale horizontally the ec2 instances or auto scaling for ECS, if deployment would be done with containers.

## To DO (tech debt)

- Using docker-compose, validate the yaml to run the application at one shot inside containers. Currently, the backend service doesnt see the postgress container and can't connect to it. This errors occurs as well when we run the Dockerfile from the /backend directory.
- Using esbuilder, validate the script to do the bundler of the application to production.
- Should improve typing with Typescript for some variables, classes and functions.
- Improve units tests and do some integration tests in the application, specifically with the middleware validation, using celebrate.
- If the frontend would be implemented, should be used CORS in the application(recommended).
- Should be implemented mock jest. Currently we are using connection directly to database and sometimes, due with the connection directly with the db, tests fail in step beforeEach.
