require("dotenv").config();
import express, { NextFunction, Request, Response } from 'express';
import { errors } from 'celebrate';

import logger from './lib/pino';
import routesBot from './modules/bot/routes/routes';
import AppError from './lib/error';
import testConnectionDb from './lib/testConnectionDb';
import pino from './lib/pino';

const app = express();
const PORT = process.env.APP_PORT || 8080;

app.use(express.json());
app.use(routesBot);
app.use(errors());

app.use((error: Error, request: Request, response: Response, next: NextFunction) => {
  if (error instanceof AppError) {
    return response.status(error.statusCode).json({
      errorCode: error.errorCode,
      message: error.message,
    });
  }
  return response.status(500).json({
    errorCde: 'UNHANDLED_ERROR',
    message: 'Internal Server Error',
  });
});

app.listen(PORT, () => {
  logger.info(`Running Express Server on port ${PORT}`);
  testConnectionDb().then(() => {
    pino.info('Database: Postgres connected')
    pino.info(`${process.env.DATABASE_URL}`)
  }).catch(async (e) => {
    pino.fatal('Database: Failed to connected to Postgres')
    pino.info(`${process.env.DATABASE_URL}`)
  })
});
