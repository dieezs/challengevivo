import pino from 'pino';

export default pino({
  transport: {
    target: 'pino-pretty',
    options: {
      ignore: 'pid,hostname',
    },
  },
});

// logger.info('this is at info level');
// logger.error('this is at error level');
// logger.fatal('this is at fatal level');
