import prismaClient from '../../config/prismaClient';

const connectDB = async () => {
  await prismaClient.bot.findMany();
}

export default connectDB;