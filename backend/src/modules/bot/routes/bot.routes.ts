import { Router } from 'express';
import BotsController from '../controllers/bots.controller';
import { celebrate, Joi, Segments } from 'celebrate';

const router = Router();
const botsController = new BotsController();

router.get(
  '/bots/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
  }),
  botsController.get,
);

router.post(
  '/bots',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
    },
  }),
  botsController.post,
);

router.put(
  '/bots/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
    [Segments.BODY]: {
      name: Joi.string().required(),
    },
  }),
  botsController.put,
);

router.delete(
  '/bots/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
  }),
  botsController.delete,
);

export default router;
