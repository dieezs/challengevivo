import { Router } from 'express';
import MessagesController from '../controllers/messages.controller';
import { celebrate, Joi, Segments } from 'celebrate';

const router = Router();
const messagesController = new MessagesController();

router.get(
  '/messages/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
  }),
  messagesController.getMessageById,
);

router.get(
  '/messages',
  celebrate({
    [Segments.QUERY]: {
      conversationId: Joi.string().uuid().required(),
    },
  }),
  messagesController.getMessageByConversationId,
);

router.post(
  '/messages',
  celebrate({
    [Segments.BODY]: {
      conversationId: Joi.string().uuid().required(),
      from: Joi.string().uuid().required(),
      to: Joi.string().uuid().required(),
      text: Joi.string().required(),
    },
  }),
  messagesController.post,
);

export default router;
