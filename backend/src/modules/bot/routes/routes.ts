import { Router } from 'express';
import BotsRoutes from '../routes/bot.routes';
import MessagesRoutes from '../routes/messages.routes';

const router = Router();

router.use(BotsRoutes);
router.use(MessagesRoutes);

export default router;
