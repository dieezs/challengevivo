import { Request, Response, NextFunction } from 'express';
import PostBot from '../../services/postBot';
import GetBotById from '../../services/getBotById';
import PutBot from '../../services/putBot';
import DeleteBot from '../../services/deleteBot';

export default class OrderController {
  public async get(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
    try {
      const{ id } = request.params
      const getBot = new GetBotById()
      const bot = await getBot.execute({ id });
      return response.json(bot)
    } catch (err) {
      next(err)
    }
  }

  public async post(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
    try {
      const{ name } = request.body
      const postBot = new PostBot()
      const bot = await postBot.execute({ name });
      return response.json(bot);
    } catch (err) {
      next(err)
    }
  }

  public async put(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
    try {
      const{ name } = request.body
      const{ id } = request.params
      const putBot = new PutBot()
      const bot = await putBot.execute({ id, name });
      return response.json(bot);
    } catch (err) {
      next(err)
    }
  }

  public async delete(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
    try {
      const{ id } = request.params
      const deleteBot = new DeleteBot()
      const bot = await deleteBot.execute({ id });
      return response.json(bot);
    } catch (err) {
      next(err)
    }
  }
}
