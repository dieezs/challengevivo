import { Request, Response, NextFunction } from 'express';
import PostMessage from '../../services/postMessage';
import GetMessageById from '../../services/getMessagesById';
import GetMessagesByConversationId from '../../services/getMessagesByConversationId';

export default class OrderController {
  public async getMessageByConversationId(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
    try {
      const { conversationId } = request.query
      const id = conversationId?.toString() || '';
      const getMessage = new GetMessagesByConversationId()
      const message = await getMessage.execute({ id });
      return response.json(message)
    } catch (err) {
      next(err)
    }
  }
  public async getMessageById(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
   try {
      const{ id } = request.params
      const getMessage = new GetMessageById()
      const message = await getMessage.execute({ id });
      return response.json(message)
    } catch (err) {
      next(err)
    }
  }

  public async post(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
     try {
      const {conversationId, from, to, text} = request.body

      const postMessage = new PostMessage()
      const message = await postMessage.execute({ conversationId, from, to, text });
      return response.json(message);
    } catch (err) {
      next(err)
    }
  }
}
