import prismaClient from '../../../config/prismaClient'

interface IRequest {
  name: string;
}

interface IResponse {
  id: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
}

class GetBotByName{
  public async execute({ name }: IRequest): Promise <IResponse | {}> {
    const bot = await prismaClient.bot.findFirst({ where: { name } })
    return bot != null ? bot : {}
  }
}

export default GetBotByName;