import prismaClient from '../../../config/prismaClient'

interface IRequest {
  id: string;
}

interface IResponse {
  id: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
}

class GetBotById{
  public async execute({ id }: IRequest): Promise <IResponse | {}> {
    const bot = await prismaClient.bot.findUnique({ where: { id } })
    return bot != null ? bot : {}
  }
}

export default GetBotById;