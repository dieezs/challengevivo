import GetMessageById from './getMessagesById';
import PostMessage from './postMessage';
import PostBot from './postBot';
import prismaClient from '../../../config/prismaClient'

describe('get message', () => {

  beforeEach(async () => {
    await prismaClient.message.deleteMany({})
    await prismaClient.bot.deleteMany({})
  })

  test('it should get a message given an id', async () => {
    const postBot = new PostBot()
    const postMessage = new PostMessage()
    const getMessageById = new GetMessageById()
    
    const bot = await postBot.execute({ name: "Ronaldinho da Copa do Mundo" });
    const messageBody = {
      conversationId: "e1348b31-51c8-4747-b05f-709d032c5a0d",
      from : "f8b923c1-3595-4f44-aa83-c4a759a0f6dc",
      to: bot.id,
      text: "Gostaria de consultar valores dos planos de celular."
    }
    const message = await postMessage.execute(messageBody);
    const messageId: any = await getMessageById.execute({id: message.id})
    expect(message.id).toBe(messageId.id)
  })
})