import PostBot from './postBot';
import DeleteBot from './deleteBot';
import prismaClient from '../../../config/prismaClient'
import AppError from '../../lib/error';

describe('get bot', () => {

  beforeEach(async () => {
    await prismaClient.message.deleteMany({})
    await prismaClient.bot.deleteMany({})
  })

  test('it should delete a bot given an id', async () => {
    const postBot = new PostBot()
    const deleteBot = new DeleteBot()
    
    const bot = await postBot.execute({ name: "Igu" });
    expect(bot).toBeTruthy()
    
    const deleteBotResult = await deleteBot.execute({ id: bot.id })
    expect(deleteBotResult.statusCode).toBe('SUCCESS')

  })  

   test('it shouldnt delete a bot with an unexistent id', async () => {
    const deleteBot = new DeleteBot()    
    try {
       await deleteBot.execute({ id: 'e1348b31-51c8-4747-b05f-709d032c5a0d' })
    } catch (error) {
      expect(error).toBeInstanceOf(AppError)
    }
  })  
  
})