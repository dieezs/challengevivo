import prismaClient from '../../../config/prismaClient'
import AppError from '../../lib/error';
import GetBotById from './getBotById';
interface IRequest {
  id: string;
}

interface IResponse {
  statusCode: string;
  message: string;
}

class PutBot{
  public async execute({ id }: IRequest): Promise<IResponse> {
        
    const getBotById = new GetBotById();
    const checkBotAlreadyExistsById = await getBotById.execute({ id })
    if (Object.keys(checkBotAlreadyExistsById).length === 0) {
      throw new AppError(400, 'BOT_DOESNT_EXISTS', 'Bot doesnt exists on database. Try to pick a different bot.');  
    }

    const bot = await prismaClient.bot.delete({
      where: { id },
    })

    return {
      statusCode: "SUCCESS",
      message: `Success to delete: ${bot.name}`
    };
  }
}

export default PutBot;