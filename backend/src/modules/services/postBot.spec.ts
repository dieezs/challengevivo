import PostBot from './postBot';
import prismaClient from '../../../config/prismaClient'
import AppError from '../../lib/error'

describe('post bot', () => {

  beforeEach(async () => {
    await prismaClient.message.deleteMany({})
    await prismaClient.bot.deleteMany({})
  })

  test('it should create a new bot', async () => {
    const postBot = new PostBot()
    const bot = await postBot.execute({ name: "Alecrim Dourado" });
    expect(bot.name).toBe('Alecrim Dourado')
  })

  test('it shouldnt create a bot with a bot already in the database with the same name', async() => {
    const postBot = new PostBot()
    try {
      await postBot.execute({ name: "Rick And Morty" });
      await postBot.execute({ name: "Rick And Morty" });
    } catch (error) {
      expect(error).toBeInstanceOf(AppError)
    }
  })
})