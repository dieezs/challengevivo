import prismaClient from '../../../config/prismaClient'
import AppError from '../../lib/error';
import GetBotById from './getBotById';

interface IRequest {
  conversationId: string;
  from: string;
  to: string;
  text: string;
}

interface IResponse{
  id: string
  conversationId: string;
  from: string;
  to: string;
  text: string;
  timestamp: Date;
  createdAt: Date;
  updatedAt: Date;
}

class PostMessage{
  public async execute({ conversationId, from, to, text }: IRequest): Promise<IResponse> {
    
    const getBot = new GetBotById();
    const checkBotExists: any = await getBot.execute({ id: to })
    if (Object.keys(checkBotExists).length === 0) {
      throw new AppError(400, 'BOT_DOESNT_EXISTS', 'Bot doesnt exists in database. Try to pick a different one.');  
    }
    
    const message = await prismaClient.message.create({
      data: {
        conversationId, text, from, toBot: {
          connect: {
            id: checkBotExists.id
          }
        },
      },
      include: {
        toBot: true
      }
    })
    return message;
  }
}

export default PostMessage