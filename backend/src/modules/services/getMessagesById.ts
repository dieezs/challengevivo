import prismaClient from '../../../config/prismaClient'

interface IRequest {
  id: string;
}

interface IResponse {
  id: string
  conversationId: string;
  from: string;
  to: string;
  text: string;
  timestamp: Date;
  createdAt: Date;
  updatedAt: Date;
  toBot: {
    name: string;
    updatedAt: Date,
    createdAt: Date,
  }
}

class GetMessagesById{
  public async execute({ id }: IRequest): Promise <IResponse | {}> {
    const bot = await prismaClient.message.findFirst({ where: { id }, include: {toBot: true} })
    return bot != null ? bot : {}
  }
}

export default GetMessagesById;