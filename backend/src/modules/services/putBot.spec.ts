import PutBot from './putBot';
import PostBot from './postBot';
import prismaClient from '../../../config/prismaClient'
import AppError from '../../lib/error';

describe('post message', () => {

  beforeEach(async () => {
    await prismaClient.message.deleteMany({})
    await prismaClient.bot.deleteMany({})
  })

  test('it should create and update the name of a given bot', async () => {
    const postBot = new PostBot()
    const pustBot = new PutBot()

    const bot: any = await postBot.execute({ name: "YunkVino" });
    expect(bot.name).toBe('YunkVino')
    const botUpdated = await pustBot.execute({id: bot.id, name: "Yunk Vino" });
    expect(botUpdated.name).toBe('Yunk Vino')
   
  })

  test('it shouldnt update a bot with a given name already in the database', async () => {
    const postBot = new PostBot()
    const pustBot = new PutBot()

    try {
      const bot: any = await postBot.execute({ name: "Bones" });
      expect(bot.name).toBe('Bones')
      await pustBot.execute({id: bot.id, name: "Bones" });
    } catch (error) {
      expect(error).toBeInstanceOf(AppError)
    }

  })

  test('it shouldnt update a bot with a given bot id that didnt exist', async () => {
    const pustBot = new PutBot()
    try {
      await pustBot.execute({id: 'e1348b31-51c8-4747-b05f-709d032c5a0d', name: "Bones" });
    } catch (error) {
      expect(error).toBeInstanceOf(AppError)
    }
  })

})