import prismaClient from '../../../config/prismaClient'
import AppError from '../../lib/error';
import GetBotByName from './getBotByName';
import GetBotById from './getBotById';

interface IRequest {
  name: string;
  id: string;
}

interface IResponse {
  id: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
}

class PutBot{
  public async execute({ id, name }: IRequest): Promise<IResponse> {
    
    const getBotByName = new GetBotByName();
    const getBotById = new GetBotById();
    const checkBotAlreadyExistsByName = await getBotByName.execute({ name })
    const checkBotAlreadyExistsById = await getBotById.execute({ id })
    if (Object.keys(checkBotAlreadyExistsByName).length >= 1) {
      throw new AppError(400, 'BOT_ALREADY_EXISTS', 'Bot already exists on database. Try to pick different name');  
    }

    if (Object.keys(checkBotAlreadyExistsById).length === 0) {
      throw new AppError(400, 'BOT_DOESNT_EXISTS', 'Bot doesnt exists on database. Try to pick a different bot.');  
    }
    
    const bot = await prismaClient.bot.update({
      where: { id },
      data: {
        name
      }
    })
    return bot;
  }
}

export default PutBot;