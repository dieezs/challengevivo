import PostBot from './postBot';
import GetBotById from './getBotById';
import GetBotByName from './getBotByName';
import prismaClient from '../../../config/prismaClient'

describe('get bot', () => {

  beforeEach(async () => {
    await prismaClient.message.deleteMany({})
    await prismaClient.bot.deleteMany({})
  })

  test('it should get a bot given an id', async () => {
    const postBot = new PostBot()
    const getBot = new GetBotById()
    const bot = await postBot.execute({ name: "Alexa" });
    const botResult: any = await getBot.execute({id: bot.id})
    expect(botResult.name).toBe('Alexa')
  })

  test('it should get a bot given a name', async () => {
    const postBot = new PostBot()
    const getBot = new GetBotByName()
    const bot = await postBot.execute({ name: "Morbius" });
    const botResult: any = await getBot.execute({name: bot.name})
    expect(botResult.name).toBe('Morbius')
  })
  
  
})