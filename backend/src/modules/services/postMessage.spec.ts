import PostMessage from './postMessage';
import PostBot from './postBot';
import prismaClient from '../../../config/prismaClient'
import AppError from '../../lib/error'

describe('post message', () => {

  beforeEach(async () => {
    await prismaClient.message.deleteMany({})
    await prismaClient.bot.deleteMany({})
  })

  test('it should create a new message with success', async () => {
    const postMessage = new PostMessage()
    const postBot = new PostBot()
    const bot = await postBot.execute({ name: "Alecrim Dourado" });
    const messageBody = {
      conversationId: "e1348b31-51c8-4747-b05f-709d032c5a0d",
      from : "f8b923c1-3595-4f44-aa83-c4a759a0f6dc",
      to: bot.id,
      text: "Gostaria de consultar valos do vivo fibra."
    }
    const message = await postMessage.execute(messageBody);
    expect(message).toBeTruthy()
   
  })

  test('it shouldnt create a message with a bot that doesnt exist.', async() => {
    const postMessage = new PostMessage()
    const messageBody = {
      conversationId: "e1348b31-51c8-4747-b05f-709d032c5a0d",
      from : "f8b923c1-3595-4f44-aa83-c4a759a0f6dc",
      to: "f8b923c1-3595-4f44-aa83-c4a759a0f6dc",
      text: "Gostaria de consultar valos do vivo fibra."
    }

    try {
      await postMessage.execute(messageBody);
    } catch (error) {
      expect(error).toBeInstanceOf(AppError)
    }
    
  })
})