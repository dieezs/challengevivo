import prismaClient from '../../../config/prismaClient'
import GetBotByName from './getBotByName'
import AppError from '../../lib/error';

interface IRequest {
  name: string;
}

interface IResponse {
  id: string;
  name: string;
}

class PostBot{
  public async execute({ name }: IRequest): Promise<IResponse> {
    
    const getBot = new GetBotByName();
    const checkBotAlreadyExists = await getBot.execute({ name })
    if (Object.keys(checkBotAlreadyExists).length >= 1) {
      throw new AppError(400, 'BOT_ALREADY_EXISTS', 'Bot already exists on database. Try a different name');
    }
    
    const bot = await prismaClient.bot.create({
      data: {
        name
      }
    })
    return bot;
  }
}

export default PostBot;