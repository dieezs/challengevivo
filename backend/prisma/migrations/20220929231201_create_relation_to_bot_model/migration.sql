/*
  Warnings:

  - You are about to drop the column `to` on the `Message` table. All the data in the column will be lost.
  - Added the required column `toBot` to the `Message` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Message" DROP COLUMN "to",
ADD COLUMN     "toBot" TEXT NOT NULL;

-- AddForeignKey
ALTER TABLE "Message" ADD CONSTRAINT "Message_toBot_fkey" FOREIGN KEY ("toBot") REFERENCES "Bot"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
