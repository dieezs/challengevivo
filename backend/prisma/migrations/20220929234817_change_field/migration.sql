/*
  Warnings:

  - You are about to drop the column `toBot` on the `Message` table. All the data in the column will be lost.
  - Added the required column `to` to the `Message` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "Message" DROP CONSTRAINT "Message_toBot_fkey";

-- AlterTable
ALTER TABLE "Message" DROP COLUMN "toBot",
ADD COLUMN     "to" TEXT NOT NULL;

-- AddForeignKey
ALTER TABLE "Message" ADD CONSTRAINT "Message_to_fkey" FOREIGN KEY ("to") REFERENCES "Bot"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
